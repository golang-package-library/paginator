package paginator

import (
	"database/sql"
	"fmt"
	"math"

	env "gitlab.com/golang-package-library/env"
	logger "gitlab.com/golang-package-library/logger"
)

const (
	defaultLimit = 10
	maxLimit     = 100
	defaultOrder = ""
	defaultSort  = "DESC"
)

// Pagination
type Pagination struct {
	CurrentPage int `json:"current_page"`
	LastPage    int `json:"last_page"`
	Total       int `json:"total"`
	PerPage     int `json:"per_page"`
}

// Paginator
type Paginator struct {
	DB         *sql.DB
	zapLogger  logger.Logger
	Pagination Pagination
}

// Options
type Options struct {
	Table  string
	Filter string
	Page   int
	Limit  int
	Sort   string
	Order  string
}

// NewPaginate
func NewPaginate(env env.Env, zapLogger logger.Logger) Paginator {

	username := env.DBUsername
	password := env.DBPassword
	host := env.DBHost
	port := env.DBPort
	dbname := env.DBName

	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", username, password, host, port, dbname)

	db, err := sql.Open("mysql", url)

	if err != nil {
		zapLogger.Zap.Info("Url: ", url)
		zapLogger.Zap.Info("Mysql Paginator Connection Refused")
		zapLogger.Zap.Panic(err)
	}

	zapLogger.Zap.Info("Mysql Paginator Connection Established")

	return Paginator{
		DB: db,
	}
}

// Config
func (p *Paginator) Config(options Options) Options {
	return Options{
		Table:  options.Table,
		Filter: options.Filter,
		Page:   options.Page,
		Limit:  options.Limit,
		Sort:   options.Sort,
		Order:  options.Order,
	}
}

// Controller
func (p *Paginator) Controller(options Options) Pagination {
	pagination := p.Service(options)
	return pagination
}

// Service
func (p *Paginator) Service(options Options) Pagination {
	offset, page, limit, order, sort := p.SetPaginationParameter(options.Page, options.Limit, options.Order, options.Sort)

	total, err := p.Repository(options.Table, options.Filter, offset, limit, order, sort)
	if err != nil {
		fmt.Println(err.Error())
	}

	pagination := p.SetPaginationResponse(page, limit, total)

	return pagination
}

// Repository
func (p *Paginator) Repository(table string, filter string, offset int, limit int, order string, sort string) (total int, err error) {
	query := "SELECT count(*) as total FROM " + table + filter
	err = p.DB.QueryRow(query).Scan(&total)

	result := float64(total) / float64(limit)
	total = int(math.Ceil(result))
	return total, err
}

// SetPaginationParameter
func (p *Paginator) SetPaginationParameter(page, limit int, order, sort string) (int, int, int, string, string) {
	if page <= 0 {
		page = 1
	}

	if limit <= 0 || limit > maxLimit {
		limit = defaultLimit
	}

	if order == "" {
		order = defaultOrder
	}

	if sort == "" {
		sort = defaultSort
	}

	offset := (page - 1) * limit

	return offset, page, limit, order, sort
}

// SetPaginationResponse
func (p *Paginator) SetPaginationResponse(page, limit, total int) Pagination {
	var lastPage int

	if total > 0 {
		lastPage = total / limit
		if total%limit != 0 {
			lastPage++
		}
	}

	return Pagination{
		CurrentPage: page,
		LastPage:    lastPage,
		Total:       total,
		PerPage:     limit,
	}
}
